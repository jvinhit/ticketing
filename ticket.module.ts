import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketComponent } from './ticket/ticket.component';

@NgModule({
  declarations: [TicketComponent],
  imports: [
    CommonModule
  ]
})
export class TicketModule { }
